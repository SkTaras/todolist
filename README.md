# ToDoList

A simple ToDoList (React.js, Node.js, Express.js and sequelize.js in combination with a MySQL database).

## Usage

1) Clone repo

```sh
git clone https://SkTaras@bitbucket.org/SkTaras/todolist.git
```

1) install dependencies on server and client

```sh
cd server/
npm install
cd..
cd client/
npm install
```

3) create database

```sh
node_modules/.bin/sequelize db:create
```

4) start express server on 3000 port

```sh
cd server/
npm start
```

5) init template data for database
   Go to link <http://localhost:3000/api/v1/init>

6) start react client on port 3001
```sh
cd client/
npm start
```

7) open client on browser
    Go to link <http://localhost:3001>