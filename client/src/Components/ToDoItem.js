import React, {Component} from 'react'

class ToDoItem extends Component {
        constructor(props) {
            super(props)
            this.state={
                description:this.props.description,
                id:this.props.id,
                isEdit:false
            }
            this.deleteItem = this.deleteItem.bind(this);
            this.handleDelete = this.handleDelete.bind(this);
            this.handleChange = this.handleChange.bind(this);
            this.handleEdit = this.handleEdit.bind(this);
        }   
        deleteItem(e){
            let self = this;
            e.stopPropagation();
            //console.log(id);
            fetch('http://localhost:3000/api/v1/todoitems/'+this.state.id, {
                method: 'DELETE',
            }).then(function(response) {
                if (response.status >= 400) {
                  throw new Error("Bad response from server");
                }
                self.handleDelete(self.state.id)
                return response.json();
            }).catch(function(err) {
                console.log(err)
            });
        }

        handleEdit(e) {
            //Edit functionality
            e.preventDefault()
            var data = {
                description: this.state.description
            }
            fetch('http://localhost:3000/api/v1/todoitems/'+this.state.id, {
                method: 'PUT',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(data)
            }).then(function(response) {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            }).then(function(response) {
               console.log(response);
                return response;
            }).catch(function(err) {
                console.log(err)
            });
        }
        
        handleDelete = (index) => {
            this.props.handleRemove(index);
            console.log('done');
        }
        handleChange(e) {
            this.setState({ description: e.target.value });
          }
        render() {
            return (
                <li className="list-group-item">
                <div className="input-group">
                    <input onChange = {this.handleChange} type="text" className="form-control" value={this.state.description}></input>
                    <span className="input-group-btn">
                    <button onClick = {this.handleEdit} className="btn btn-info" type="button">Edit</button>
                    <button onClick = {this.deleteItem} className="btn btn-danger" type="button">Delete</button>
                    </span>
                </div>
                </li>
            );
        }
    }
export default ToDoItem