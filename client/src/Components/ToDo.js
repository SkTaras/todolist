import React, {Component} from 'react'
import ToDoItems from './ToDoItems'

class ToDo extends Component {
        constructor(props) {
            super(props)
            this.state ={
                isOpen:false,
                id:this.props.toDoId
            }
            this.toggleOpen = this.toggleOpen.bind(this);
            this.deleteItem = this.deleteItem.bind(this);
            this.handleDelete = this.handleDelete.bind(this);
        }  

        deleteItem(e){
            let self = this;
            e.stopPropagation();
            //console.log(id);
            fetch('http://localhost:3000/api/v1/todos/'+this.state.id, {
                method: 'DELETE',
            }).then(function(response) {
                if (response.status >= 400) {
                  throw new Error("Bad response from server");
                }
                self.handleDelete(self.state.id)
                return response.json();
            }).catch(function(err) {
                console.log(err)
            });
        }
        handleDelete = (index) => {
            this.props.handleRemove(index);
            console.log('done');
        }
        render() {
            return (
                <li className="list-group-item">
                <div className="input-group">
                    <label onClick = {this.toggleOpen} className="form-control">{this.props.title}</label>
                    <span className="input-group-btn">
                            <button onClick={this.deleteItem} className="btn btn-danger" type="button">Delete</button>
                    </span>
                    {this.getBody()} 
                </div>
                </li>
            );
        }

    getBody(){
        if (!this.state.isOpen) return null;
        return <ToDoItems items = {this.props.items} toDoId = {this.props.toDoId} pushItems = {this.props.pushItems}></ToDoItems>
    }
    toggleOpen = () => {
        this.setState({
            isOpen:!this.state.isOpen
        })
    }
    }
export default ToDo