import React, {Component} from 'react'
import ToDoItem from './ToDoItem'

class ToDoItems extends Component {
        constructor(props) {
            super(props)
            this.state={
                items : this.props.items,
                input : ""
            }
            this.handleRemove = this.handleRemove.bind(this);
            this.handleAdd = this.handleAdd.bind(this);
            this.handleChange = this.handleChange.bind(this); 
        } 
        componentWillReceiveProps(nextProps) {
            if (nextProps.items !== this.state.items) {
              this.setState({ items: nextProps.items });
            }
          }   
        handleRemove(index){
           this.setState({
           items: this.state.items.filter(item => item.id !== index)
          });
          console.log(this.state.items);
        }

        handleChange = (e) => {
            e.stopPropagation();
            this.setState({ input: e.target.value });
            console.log(this.state.input);
          };
        handleAdd = (e) =>{
            let self = this;
            e.preventDefault()
            var data = {
                description: this.state.input,
                todoId: this.props.toDoId
            }
            console.log(e.target.value);
            fetch("http://localhost:3000/api/v1/todoitems", {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(data)
            }).then(function(response) {
                if (response.status >= 400) {
                  throw new Error("Bad response from server");
                }
                //console.log(response.json());
                return response.json();
            }).then(() =>{
                self.pushItems();
            });
        }
        pushItems = () =>{
            this.props.pushItems();
        }

        render() {
            return (
                <div>
                    <ul id="sortable" className="list-group">
                    {this.state.items.map(items => 
                        <ToDoItem key={items.id} 
                        description={items.description} 
                        id={items.id} 
                        handleRemove = {this.handleRemove}
                        updateData = {this.updateData}>
                        </ToDoItem> )}
                    </ul>
                    <div className="input-group">
                        <input onChange={this.handleChange} type="text" className="form-control"></input>
                        <span className="input-group-btn">
                            <button onClick={this.handleAdd} className="btn btn-success" type="button">Add Item</button>
                        </span>
                    </div>
             </div>
            );
        }
    }
export default ToDoItems