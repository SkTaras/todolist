import React, {Component} from 'react'
import ToDo from './ToDo'

class ToDoList extends Component {
        constructor(props) {
            super(props)
            this.state = {
                toDos: [],
                isOpen: false,
                input: "",
                sort: "DESC"
            }
            this.handleAdd = this.handleAdd.bind(this);
            this.handleChange = this.handleChange.bind(this);  
            this.handleRemove = this.handleRemove.bind(this);
            this.fetchAll = this.fetchAll.bind(this);
            this.changeSort = this.changeSort.bind(this);
        }

        fetchAll(){
            let self = this;
            fetch('http://localhost:3000/api/v1/todos/?sort='+encodeURIComponent(this.state.sort), {
                method: 'GET'
            }).then(function(response) {
                if (response.status >= 400) {
                    throw new Error("Bad response from server");
                }
                return response.json();
            }).then(function(data) {
                self.setState({toDos: data});
                console.log(self.state.toDos);
            }).catch(err => {
            console.log('caught it!',err);
            })
        }

        componentDidMount() {
           this.fetchAll();
        }

        handleChange(e) {
            this.setState({ input: e.target.value });
            console.log(this.state.input);
          };

        handleAdd(e){
            let self = this;
            e.preventDefault()
            var data = {
                title: this.state.input
            }
            console.log(e.target.value);
            fetch("http://localhost:3000/api/v1/todos", {
                method: 'POST',
                headers: {'Content-Type': 'application/json'},
                body: JSON.stringify(data)
            }).then(function(response) {
                if (response.status >= 400) {
                  throw new Error("Bad response from server");
                }
                return response.json();
            }).then(function(response){
                self.fetchAll();
                console.log(response);
            }
            ).catch(function(err) {
                console.log(err)
            });
        }

        pushItems = () => {
            this.fetchAll();
        }

        handleRemove(index){
            this.setState({
            toDos: this.state.toDos.filter(toDo => toDo.id !== index)
           });
         }

         changeSort = () => {
             this.setState({
                 sort: (this.state.sort==="ASC")?"DESC":"ASC"
             });
             this.fetchAll();
         }
    
        render() {
            return ( 
                <div className="todolist not-done">
                    <h1>Todos</h1> 
                    <div className="input-group">
                    <input onChange={this.handleChange} ref="addInput" type="text" className="form-control add-todo" placeholder="Add todo"/>
                        <span className="input-group-btn">
                        <button onClick={this.handleAdd} className="btn btn-success">Add ToDo</button>
                        <button onClick={this.changeSort}  className="btn btn-warning">Change sort method</button>
                        </span>
                    </div>
                   
                   
                    <hr/>
                    <ul id="sortable" className="list-group">
                        {this.state.toDos.map(toDo =>
                        <ToDo key={toDo.id} 
                        title={toDo.title} 
                        items={toDo.ToDoItems} 
                        toDoId={toDo.id} 
                        pushItems={this.pushItems}
                        handleRemove={this.handleRemove}></ToDo> )}
                    </ul>
                </div>
            );
        }
        toggleOpen = () => {
            this.setState({
                isOpen: !this.state.isOpen
            })
        }
    }
export default ToDoList