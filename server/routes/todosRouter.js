import todosController from '../controllers/todosController';
import todoItemController from '../controllers/toDoItemController';

module.exports = (router) => {

    //For init db data
    router.route('/init').get(todosController.init)
    
    //ToDos
    router.route('/todos').get(todosController.getAll)
    router.route('/todos').post(todosController.add)
    router.route('/todos/:id').get(todosController.get)
    router.route('/todos/:id').put(todosController.edit)
    router.route('/todos/:id').delete(todosController.delete)
    //ToDoItems
    router.route('/todoitems').get(todoItemController.getAll)
    router.route('/todoitems').post(todoItemController.add)
    router.route('/todoitems/:id').get(todoItemController.get)
    router.route('/todoitems/:id').put(todoItemController.edit)
    router.route('/todoitems/:id').delete(todoItemController.delete)
}