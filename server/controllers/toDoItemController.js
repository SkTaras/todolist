import db from '../models';

module.exports = {
  getAll:(req, res) =>{
    return db.ToDoItem.findAll()
      .then((ToDoItem) => {res.send(ToDoItem);})
      .catch((err) => {
        console.log('There was an error', JSON.stringify(err))
        return res.send(err)
      });
  },

  get:(req, res) =>{
    const id = parseInt(req.query.id)
    return db.ToDoItem.findById(id)
      .then((ToDoItem) => {res.send(ToDoItem);})
      .catch((err) => {
        console.log('There was an error', JSON.stringify(err))
        return res.send(err)
      });
  },

  add:(req, res) => {
    const { description, todoId } = req.body
    return db.ToDoItem.create({ description, todoId })
    .then((ToDoItem) => res.send(ToDoItem))
    .catch((err) => {
      console.log('***There was an error creating a contact', JSON.stringify(contact))
      return res.status(400).send(err)
    })},

  edit:(req,res) => {
    const id = parseInt(req.params.id)
    return db.ToDoItem.findById(id)
    .then((ToDoItem) => {
      const { description } = req.body
      return ToDoItem.update({ description })
        .then(() => res.send(ToDoItem))
        .catch((err) => {
          console.log('***Error updating contact', JSON.stringify(err))
          res.status(400).send(err)
        })
    })
  },

  delete:(req,res) => {
    const id = parseInt(req.params.id)
    return db.ToDoItem.findById(id)
      .then((ToDoItem) => ToDoItem.destroy({ force: true }))
      .then(() => res.send({ id }))
      .catch((err) => {
        console.log('***Error deleting contact', JSON.stringify(err))
        res.status(400).send(err)
      })
  },
}
