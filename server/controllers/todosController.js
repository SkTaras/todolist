import db from '../models';

module.exports = {
  getAll:(req, res) =>{
    return db.ToDo.findAll({
      include: [{
        model: db.ToDoItem,
        //where: { ToDoId: db.Sequelize.col('todo.id') }
    }],
      order:[['title',req.query.sort]]})
    .then((Todo) => {res.send(Todo);})
    .catch((err) => {
      console.log('There was an error', JSON.stringify(err))
      return res.send(err)
    });
  },

  get:(req, res) => {
    const id = parseInt(req.params.id, 10);
    return db.Todos.findById(id)
    .then((Todos) => res.send(Todos))
    .catch((err) => {
      console.log('There was an error', JSON.stringify(err))
      return res.send(err)
    });
  },

  add:(req, res) => {
    const { title } = req.body
    return db.ToDo.create({ title })
    .then((todo) => res.send(todo))
      .catch((err) => {
        console.log('***There was an error creating a ToDo', JSON.stringify(contact))
        return res.status(400).send(err)
    })},

  edit:(req,res) => {
    const id = parseInt(req.params.id)
    return db.ToDo.findById(id)
    .then((ToDo) => {
      const { description } = req.body
      return ToDo.update({ description })
        .then(() => res.send(ToDo))
          .catch((err) => {
            console.log('***Error updating ToDo', JSON.stringify(err))
            res.status(400).send(err)
          })
      })
    },

  delete:(req,res) => {
      const id = parseInt(req.params.id)
      return db.ToDo.findById(id)
        .then((ToDo) => ToDo.destroy({ force: true }))
          .then(() => res.send({ id }))
          .catch((err) => {
            console.log('***Error deleting ToDo', JSON.stringify(err))
            res.status(400).send(err)
        })
    },

  init:(req,res)=>{
    db.ToDo.bulkCreate([
      {title: "Today"},
      {title: "Tomorrow"},
      {title:"Upcoming"}
    ]).then(
      db.ToDoItem.bulkCreate([
        {description: "Email the presentation", todoId: 1},
        {description: "By a cake", todoId: 1},
        {description: "Booking hotel", todoId: 2},
        {description: "By fly ticket", todoId: 3},
      ])
    ).then(() => {
      res.send("Done!");
    })
  }
}
