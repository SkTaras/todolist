import bodyParser from 'body-parser';
import express from 'express';
import routes from './routes';
import db from './models/index';
import cors from 'cors';
  
db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync with { force: true }');
});

const app = express();
const router = express.Router()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cors());
routes(router)

app.use('/api/v1',router);

export default app;
