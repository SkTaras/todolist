'use strict';
module.exports = (sequelize, DataTypes) => {
  const ToDo = sequelize.define('ToDo', {
    title: DataTypes.STRING,
  }, {});
  ToDo.associate = function(models) {
    ToDo.hasMany(models.ToDoItem, {
      foreignKey: 'todoId',
    });
  };
  return ToDo;
};