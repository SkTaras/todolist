'use strict';
module.exports = (sequelize, DataTypes) => {
  const ToDoItem = sequelize.define('ToDoItem', {
    description: DataTypes.STRING,
  }, {});
  ToDoItem.associate = function(models) {
    // associations can be defined here
    ToDoItem.belongsTo(models.ToDo, {
      foreignKey: 'todoId',
      onDelete: 'CASCADE',
    });
  };
  return ToDoItem;
};